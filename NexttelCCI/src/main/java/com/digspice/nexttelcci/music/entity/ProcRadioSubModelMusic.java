package com.digspice.nexttelcci.music.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

@Entity
@NamedStoredProcedureQuery(
        name="PROC_RADIO_SUBS_MUSIC",
        procedureName="PROC_RADIO_SUBS",
        parameters={
    			
                @StoredProcedureParameter(name="IN_MOBILE",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_STATUS",mode=ParameterMode.IN,type=Integer.class),
                @StoredProcedureParameter(name="IN_PACK_TYPE",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_LANG",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_ACT_SRC",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_DEACT_REASON",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_PARA",mode=ParameterMode.IN,type=Integer.class),
                @StoredProcedureParameter(name="OUT_PARA",mode=ParameterMode.OUT,type=String.class),
                
                
        }
)


public class ProcRadioSubModelMusic {
	
	@Id
	private int id;
	private String name;
	
}
