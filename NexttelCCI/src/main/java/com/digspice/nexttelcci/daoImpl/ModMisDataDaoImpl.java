package com.digspice.nexttelcci.daoImpl;

import java.util.List;


import org.apache.log4j.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.digspice.nexttelcci.mobileRadio.entity.NexttelModMISEntity;



@Repository
@Transactional("tx_mod")
public class ModMisDataDaoImpl {
	
	@Autowired
	@Qualifier(value="sessionFactory")
	SessionFactory sessionFactory;
	
	Session session = null;
	
	private static final Logger LOGGER = Logger.getLogger(ModMisDataDaoImpl.class);
	
	public List<NexttelModMISEntity> getMODMisData(String fromDate,String toDate)
	{
		List<NexttelModMISEntity> listOfNexttelModMIS = null;
		try
		{
			session = sessionFactory.getCurrentSession();
			
			Query query = session.createQuery("from NexttelModMISEntity where date(date_time) between '"+fromDate+"' and '"+toDate+"'");
			LOGGER.info(query.list());
			if(query.list()!=null)
			{
				listOfNexttelModMIS = query.list();
			}
		}
		
		catch(Exception e)
		{
			LOGGER.info("Exception in getting data from MOD MIS table "+e);
		}
			
		return listOfNexttelModMIS;
	}
}
