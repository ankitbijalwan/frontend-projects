package com.digspice.nexttelcci.daoImpl;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.digspice.nexttelcci.dao.HomeActoinDao;
import com.digspice.nexttelcci.mobileRadio.entity.UserLoginEntity;

@Repository
@Transactional("tx_mod")
public class HomeActoinDaoImpl implements HomeActoinDao{
	
	private static final Logger LOGGER = Logger.getLogger(HomeActoinDaoImpl.class);

	@Autowired
	@Qualifier(value="sessionFactory")
	SessionFactory sessionFactory;
	
	Session session = null;
	
	@Override
	public UserLoginEntity checkLoginAuthentication(String username,String password)
	{
		
		UserLoginEntity userLoginEntity = null;
		try
		{
			System.out.println("sas " +sessionFactory.toString());
			session = sessionFactory.getCurrentSession();
			LOGGER.info("Checking User Authorization by Username {["+username+"]} and password {["+password+"]}");
			
			Criteria criteria = session.createCriteria(UserLoginEntity.class);
			criteria.add(Restrictions.eq("username", username));
			criteria.add(Restrictions.eq("password", password));
			Restrictions.eq("username", username);

			userLoginEntity = (UserLoginEntity)criteria.list().get(0);
					
		}
		catch(Exception e)
		{
			LOGGER.info("Invalid Credentials Username {["+username+"]} and password {["+password+"]}");
			LOGGER.info(e);
		}
		return userLoginEntity;
	}
	
}
