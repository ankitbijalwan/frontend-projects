package com.digspice.nexttelcci.daoImpl;


import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import java.util.List;

import javax.persistence.StoredProcedureQuery;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.digspice.nexttelcci.dao.ModActionDao;
import com.digspice.nexttelcci.mobileRadio.entity.ModSubDetails;
import com.digspice.nexttelcci.mobileRadio.entity.ModUnsubDetail;
import com.digspice.nexttelcci.mobileRadio.entity.UserBillingDetails;

@Repository
@Transactional("tx_mod")
public class ModActionDaoImpl implements ModActionDao{
	
	private static final Logger LOGGER = Logger.getLogger(ModActionDaoImpl.class);
			
	@Autowired
	@Qualifier(value="sessionFactory")
	SessionFactory sessionFactory;
	
	
	Session session = null;
	
	
	@Override
	public String modSinleSubscription(String ani,String pack)
	{
		String mobile = ani.trim();
		String lang = "e";
		String mode = "cci";
		int inPara = 1;
		String response = null;
		
		
		try
		{
			session = sessionFactory.getCurrentSession();
			
			StoredProcedureQuery query = session.createNamedStoredProcedureQuery("PROC_RADIO_SUBS");
			query.setParameter("IN_MOBILE", mobile);
			query.setParameter("IN_PACK_TYPE", pack);
			query.setParameter("IN_LANG", lang);
			query.setParameter("IN_MODE", mode);
			query.setParameter("IN_PARA", inPara);
		
			query.execute();
			
			response = (String)query.getOutputParameterValue("OUT_PARA");
			
		}
		catch(Exception e)
		{
			LOGGER.info("exception occured "+e);
			e.printStackTrace();
		}
		
		System.out.println("response is "+ response);
		return response;
	}
	
	
	
	
	@Override
	public List<UserBillingDetails> getModUserBillingDetails(String ani)
	{
		List<Object[]> list = null;
		List<UserBillingDetails> listUserBillingDetails = new ArrayList<UserBillingDetails>();
		try
		{
			session = sessionFactory.getCurrentSession();
			Query query = session.createSQLQuery("select IFNULL(ANI,0),IFNULL(BILL_STATUS,0),IFNULL(date(SUB_DATE),0)"
					+ ",IFNULL(date(RENEW_DATE),0),IFNULL(date(BILLING_DATE),0),IFNULL(PRE_POST,0),"
					+ "IFNULL(AMT_DEDUCTED,0),IFNULL(PACK_TYPE,0),IFNULL(ACT_SOURCE,0),IFNULL(TID,0),"
					+ "IFNULL(SERVICE_NAME,0) from tbl_mr_billing_history where ANI='"+ani+"'");
			
			list = query.list();
			LOGGER.info("Size of list is {["+list.size()+"]}");
			
			for(Object[] a:list)
			{
				UserBillingDetails user = new UserBillingDetails();
				user.setANI(a[0].toString());
				user.setBILL_STATUS(a[1].toString());
				user.setSUB_DATE(a[2].toString());
				user.setRENEW_DATE(a[3].toString());
				user.setBILLING_DATE(a[4].toString());
				user.setPRE_POST(a[5].toString());
				user.setAMT_DEDUCTED(a[6].toString());
				user.setPACK_TYPE(a[7].toString());
				user.setACT_SOURCE(a[8].toString());
				user.setTID(a[9].toString());
				user.setSERVICE_NAME(a[10].toString());
				
				if(user.getPACK_TYPE().equalsIgnoreCase("MR_SUB1"))
					user.setPACK_TYPE("Monthly");
				else if(user.getPACK_TYPE().equalsIgnoreCase("MR_SUB2"))
					user.setPACK_TYPE("Weekly");
				else if(user.getPACK_TYPE().equalsIgnoreCase("MR_SUB3"))
					user.setPACK_TYPE("Weekly");
				
				listUserBillingDetails.add(user);
				
			}
		
		}
		catch(Exception e)
		{
			LOGGER.info("Exception occured "+e);
		}
		return listUserBillingDetails;
	}
	
	
	public String bulkModSinleSubscription(String pack,String path)
	{
		String line = null;
		String lang = "e";
		String mode = "cci";
		int inPara = 1;
		String response = null;
		try
		{
			session = sessionFactory.getCurrentSession();
			
			BufferedReader bf = new BufferedReader(new FileReader(new File(path)));
			
			while((line=bf.readLine())!=null)
			{
				if(line.length()==10)
				{
					StoredProcedureQuery query = session.createNamedStoredProcedureQuery("PROC_RADIO_SUBS");
					query.setParameter("IN_MOBILE", line.trim());
					query.setParameter("IN_PACK_TYPE", pack);
					query.setParameter("IN_LANG", lang);
					query.setParameter("IN_MODE", mode);
					query.setParameter("IN_PARA", inPara);
			
					query.execute();
				
					response = (String)query.getOutputParameterValue("OUT_PARA");
					LOGGER.info("Response for ANI {["+line+"]} is {["+response+"]}");
				}
				
				else
				{
					LOGGER.info("ANI {["+line+"]} is Incorrect");
				}
			}
			
			response = "success";
		}
		
		catch(Exception e)
		{
			LOGGER.info("Exception Occured during bulk option "+e);
			e.printStackTrace();
			response = "exception";
		}
		
		return response;
	}
	
	@Override
	public ArrayList<ModUnsubDetail> modUserDetailsforUnsub(String ani)
	{
		List<Object[]> list = null;
		ArrayList<ModUnsubDetail> unsublist = new ArrayList<ModUnsubDetail>();
		try
		{
			session = sessionFactory.getCurrentSession();
			String query = null;
			
				 
				Query sqlQuery = session.createSQLQuery("SELECT IFNULL(a.MOBILE_NUMBER,0),IFNULL(a.STATUS,0),IFNULL(a.SUB_DATE,0),"
						+ "IFNULL(a.RENEW_DATE,0),IFNULL(a.BILLING_DATE,0),IFNULL(a.PRE_POST,0)"
						+ ",IFNULL(a.TIME_LIMIT,0),IFNULL(a.PACK_TYPE,0),IFNULL(a.LANGUAGE,0),"
						+ "IFNULL(a.ACT_SOURCE,0),IFNULL(a.GRACE_DAYS,0),IFNULL(a.SUBS_ID,0),IFNULL(a.TRANS_ID,0),"
						+ "IFNULL(a.USER_COUNTER,0),IFNULL(a.UNSUB_MODE,0),IFNULL(a.UNSUB_DATE,0),"
						+ "IFNULL(b.AMOUNT,0) FROM tbl_radio_unsubscriptions a LEFT JOIN tbl_billinginfo b "
						+ "ON a.PACK_TYPE = b.user_type WHERE MOBILE_NUMBER='"+ani+"'");
				
				list = sqlQuery.list();
				
				for(Object[] a:list)
				{
					ModUnsubDetail unsub = new ModUnsubDetail();
					System.out.println(a[0].toString());
					unsub.setMOBILE_NUMBER(a[0].toString());
					unsub.setSTATUS(a[1].toString());
					unsub.setSUB_DATE(a[2].toString());
					unsub.setRENEW_DATE(a[3].toString());
					unsub.setBILLING_DATE(a[4].toString());
					unsub.setPRE_POST(a[5].toString());
					unsub.setTIME_LIMIT(a[6].toString());
					unsub.setPACK_TYPE(a[7].toString());
					unsub.setLANGUAGE(a[8].toString());
					unsub.setACT_SOURCE(a[9].toString());
					unsub.setGRACE_DAYS(a[10].toString());
					unsub.setSUBS_ID(a[11].toString());
					unsub.setTRANS_ID(a[12].toString());
					unsub.setUSER_COUNTER(a[13].toString());
					unsub.setUNSUB_MODE(a[14].toString());
					unsub.setUNSUB_DATE(a[15].toString());
					unsub.setAMOUNT(a[16].toString());
					
					if(unsub.getPACK_TYPE().equalsIgnoreCase("MR_SUB1"))
						unsub.setPACK_TYPE("Monthly");
					else if(unsub.getPACK_TYPE().equalsIgnoreCase("MR_SUB2"))
						unsub.setPACK_TYPE("Weekly");
					else if(unsub.getPACK_TYPE().equalsIgnoreCase("MR_SUB3"))
						unsub.setPACK_TYPE("Weekly");
				
					unsublist.add(unsub);
				}
		}
		
		catch(Exception e)
		{
			LOGGER.info("Exception Occured in getting user Details from Unsubscrption mode "+e);
		}
		return unsublist;
	}
	
	
	@Override
	public ArrayList<ModSubDetails> modUserDetailsforsub(String ani)
	{
		List<Object[]> list = null;
		ArrayList<ModSubDetails> sublist = new ArrayList<ModSubDetails>();
		try
		{
			session = sessionFactory.getCurrentSession();
			String query = null;
			
				 
				Query sqlQuery = session.createSQLQuery("SELECT IFNULL(a.MOBILE_NUMBER,0),IFNULL(a.STATUS,0),"
						+ "IFNULL(a.SUB_DATE,0),"
						+ "IFNULL(a.RENEW_DATE,0),IFNULL(a.BILLING_DATE,0),IFNULL(a.PRE_POST,0)"
						+ ",IFNULL(a.TIME_LIMIT,0),IFNULL(a.PACK_TYPE,0),IFNULL(a.LANGUAGE,0),"
						+ "IFNULL(a.ACT_SOURCE,0),IFNULL(a.GRACE_DAYS,0),IFNULL(a.SUBS_ID,0),IFNULL(a.TRANS_ID,0),"
						+ "IFNULL(a.USER_COUNTER,0),"
						+ "IFNULL(b.AMOUNT,0) FROM tbl_radio_subs a LEFT JOIN tbl_billinginfo b "
						+ "ON a.PACK_TYPE = b.user_type WHERE a.MOBILE_NUMBER='"+ani+"'");
				
				list = sqlQuery.list();
				
				for(Object[] a:list)
				{
					ModSubDetails sub = new ModSubDetails();
					sub.setMOBILE_NUMBER(a[0].toString());
					sub.setSTATUS(a[1].toString());
					sub.setSUB_DATE(a[2].toString());
					sub.setRENEW_DATE(a[3].toString());
					sub.setBILLING_DATE(a[4].toString());
					sub.setPRE_POST(a[5].toString());
					sub.setTIME_LIMIT(a[6].toString());
					sub.setPACK_TYPE(a[7].toString());
					sub.setLANGUAGE(a[8].toString());
					sub.setACT_SOURCE(a[9].toString());
					sub.setGRACE_DAYS(a[10].toString());
					sub.setSUBS_ID(a[11].toString());
					sub.setTRANS_ID(a[12].toString());
					sub.setUSER_COUNTER(a[13].toString());
					sub.setAMOUNT(a[14].toString());
					
					if(sub.getPACK_TYPE().equalsIgnoreCase("MR_SUB1"))
						sub.setPACK_TYPE("Monthly");
					else if(sub.getPACK_TYPE().equalsIgnoreCase("MR_SUB2"))
						sub.setPACK_TYPE("Weekly");
					else if(sub.getPACK_TYPE().equalsIgnoreCase("MR_SUB3"))
						sub.setPACK_TYPE("Weekly");
				
					sublist.add(sub);
				}
		}
		
		catch(Exception e)
		{
			LOGGER.info("Exception Occured in getting user Details from Subscrption mode "+e);
			System.out.println(e);
			}
		return sublist;
	}
	
	
	
	@Override
	public String modUnSubscription(String ani)
	{
		String mobile = ani.trim();
		String lang = "e";
		String mode = "cci";
		int inPara = 4;
		String response = null;
		String pack="MR_SUB1";
		
		
		try
		{
			session = sessionFactory.getCurrentSession();
			StoredProcedureQuery query = session.createNamedStoredProcedureQuery("PROC_RADIO_SUBS");
			query.setParameter("IN_MOBILE", mobile);
			query.setParameter("IN_PACK_TYPE", pack);
			query.setParameter("IN_LANG", lang);
			query.setParameter("IN_MODE", mode);
			query.setParameter("IN_PARA", inPara);
		
			query.execute();
			
			response = (String)query.getOutputParameterValue("OUT_PARA");
			
		}
		catch(Exception e)
		{
			LOGGER.info("exception occured "+e);
			e.printStackTrace();
			response = "exception";
		}
		
		LOGGER.info("response from Procedure in Single Unsubscription is  "+ response);
		return response;
	}

}
