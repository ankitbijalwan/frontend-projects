package com.digspice.nexttelcci.daoImpl;

import java.util.List;


import org.apache.log4j.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.digspice.nexttelcci.music.entity.NexttelMusicMISEntity;



@Repository
@Transactional("tx_music")
public class MusicMisDataDaoImpl {
	
	@Autowired
	@Qualifier(value="secondSessionFactory")
	SessionFactory secondSessionFactory;
	

	Session session = null;
	
	private static final Logger LOGGER = Logger.getLogger(MusicMisDataDaoImpl.class);
	
	public List<NexttelMusicMISEntity> getMusicMisData(String fromDate,String toDate)
	{
		List<NexttelMusicMISEntity> listOfNexttelMusicMIS = null;
		
		try
		{
			session = secondSessionFactory.getCurrentSession();
			
			Query query = session.createQuery("from NexttelMusicMISEntity where date(date_time) between '"+fromDate+"' and '"+toDate+"'");
			LOGGER.info(query.list());
			if(query.list()!=null)
			{
				listOfNexttelMusicMIS = query.list();
			}
		}
		
		catch(Exception e)
		{
			LOGGER.info("Exception in getting data from MUSIC MIS table "+e);
			
		}
		
		return listOfNexttelMusicMIS;
	}

}
