package com.digspice.nexttelcci.dao;


import java.util.ArrayList;
import java.util.List;

import com.digspice.nexttelcci.mobileRadio.entity.ModSubDetails;
import com.digspice.nexttelcci.mobileRadio.entity.ModUnsubDetail;
import com.digspice.nexttelcci.mobileRadio.entity.UserBillingDetails;

public interface ModActionDao {

	public List<UserBillingDetails> getModUserBillingDetails(String ani);
	public String modSinleSubscription(String ani,String pack);
	public String bulkModSinleSubscription(String pack,String path);
	public String modUnSubscription(String ani);
	
	
	/*Fetching user details according to Detail Type*/	
	public ArrayList<ModUnsubDetail> modUserDetailsforUnsub(String ani);
	public ArrayList<ModSubDetails> modUserDetailsforsub(String ani);
}
