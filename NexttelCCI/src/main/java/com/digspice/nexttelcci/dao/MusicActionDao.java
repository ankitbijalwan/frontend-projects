package com.digspice.nexttelcci.dao;


import java.util.ArrayList;
import java.util.List;

import com.digspice.nexttelcci.mobileRadio.entity.ModSubDetails;
import com.digspice.nexttelcci.mobileRadio.entity.ModUnsubDetail;
import com.digspice.nexttelcci.mobileRadio.entity.UserBillingDetails;

public interface MusicActionDao {

	public List<UserBillingDetails> getMusicUserBillingDetails(String ani);
	public String musicSingleSubscription(String ani,String pack);
	public String bulkMusicSinleSubscription(String pack,String path);
	
/*Fetching user details according to Detail Type*/
	
	public ArrayList<ModUnsubDetail> musicUserDetailsforUnsub(String ani);
	public ArrayList<ModSubDetails> musicUserDetailsforsub(String ani);
}
