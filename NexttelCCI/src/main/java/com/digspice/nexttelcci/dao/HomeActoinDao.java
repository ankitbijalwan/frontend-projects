package com.digspice.nexttelcci.dao;

import com.digspice.nexttelcci.mobileRadio.entity.UserLoginEntity;

public interface HomeActoinDao {
	
	public UserLoginEntity checkLoginAuthentication(String username,String password);

}
