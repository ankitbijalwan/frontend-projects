package com.digspice.nexttelcci.mobileRadio.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.ParameterMode;

@Entity
@NamedStoredProcedureQuery(
        name="PROC_RADIO_SUBS",
        procedureName="PROC_RADIO_SUBS",
        parameters={
                @StoredProcedureParameter(name="IN_MOBILE",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_PACK_TYPE",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_LANG",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_MODE",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_PARA",mode=ParameterMode.IN,type=Integer.class),
                @StoredProcedureParameter(name="OUT_PARA",mode=ParameterMode.OUT,type=String.class),
                
                /*@StoredProcedureParameter(name="IN_PACK_TYPE",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_LANG",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_MODE",mode=ParameterMode.IN,type=String.class),
                @StoredProcedureParameter(name="IN_PARA",mode=ParameterMode.IN,type=Integer.class),
                @StoredProcedureParameter(name="OUT_PARA",mode=ParameterMode.OUT,type=String.class),*/
                
        }
)

public class ProcRadioSubModel {

	@Id
	private int id;
	
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
