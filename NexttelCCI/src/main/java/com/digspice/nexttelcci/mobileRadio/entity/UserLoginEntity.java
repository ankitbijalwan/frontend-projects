package com.digspice.nexttelcci.mobileRadio.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_CCI_userLogin")
public class UserLoginEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String username;
	private String password;
	private String role;
	private String accessArea;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getAccessArea() {
		return accessArea;
	}
	public void setAccessArea(String accessArea) {
		this.accessArea = accessArea;
	}
	@Override
	public String toString() {
		return "UserLoginEntity [id=" + id + ", username=" + username + ", password=" + password + ", role=" + role
				+ ", accessArea=" + accessArea + "]";
	}
	
	
	
	
}
