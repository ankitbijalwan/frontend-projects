package com.digspice.nexttelcci.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digspice.nexttelcci.dao.MusicActionDao;
import com.digspice.nexttelcci.mobileRadio.entity.ModSubDetails;
import com.digspice.nexttelcci.mobileRadio.entity.ModUnsubDetail;
import com.digspice.nexttelcci.mobileRadio.entity.UserBillingDetails;

@Controller
public class MusicController {
	
	@Autowired
	HttpSession session;
	
	@Autowired
	MusicActionDao musicActionDao;
	
	private static final Logger LOGGER = Logger.getLogger(MusicController.class);
	
	private static final String UPLOAD_DIRECTORY = "E:\\NexttelMod\\Bulk_MOD_Files\\";
	String metaPath = null;
	
	
	@RequestMapping(value="music_UserDetails")
	public String musicUserDetails()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("User Details for Service {[MUSIC SCORER}]");
			return "music-userDetails";
		}
	}
	
	
	@RequestMapping(value="music_UserDetailsAction",method=RequestMethod.POST)
	public String musicUserDetailsAction(@RequestParam String detail_type,@RequestParam String ani,RedirectAttributes redir)
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("Fetching user detail for Service {[MUSIC SCORER}] ANI {["+ani+"]} for type {["+detail_type+"]}");
			if(detail_type.equalsIgnoreCase("subscription"))
			{
				ArrayList<ModSubDetails> subdetail= musicActionDao.musicUserDetailsforsub(ani);
				LOGGER.info("No. of records in Subcription table is {["+subdetail.size()+"]}");
				if(subdetail.isEmpty()==false)
				{
					redir.addFlashAttribute("subdata", subdetail);
				}
				else
				{
					redir.addFlashAttribute("msg", "No subscription Detail found for "+ani);
				}
			}
			else if(detail_type.equalsIgnoreCase("un-subscription"))
			{
				ArrayList<ModUnsubDetail> unsubdetail= musicActionDao.musicUserDetailsforUnsub(ani);
				LOGGER.info("No. of records in UnSubcription table is {["+unsubdetail.size()+"]}");
				if(unsubdetail.isEmpty()==false)
				{
					redir.addFlashAttribute("unsubdata", unsubdetail);
				}
				else
				{
					redir.addFlashAttribute("msg", "No Unsubscription Detail found for "+ani);
				}
			}
			else if(detail_type.equalsIgnoreCase("download"))
			{

			}
			else if(detail_type.equalsIgnoreCase("dedication"))
			{
				
			}
				
			else if(detail_type.equalsIgnoreCase("playlist"))
			{
			
			}
			
			return "redirect:/music_UserDetails";
		}
	}
	
	
	@RequestMapping(value="music_SingleSubscription")
	public String musicSingleSubscription()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("Single Subscription for Service {[MUSIC SCORER}]");
			return "music-singleSubscription";
		}
	}
	
	
	
	@RequestMapping(value="music_SingleSubscriptionAction")
	public String modSingleSubscriptionAction(@RequestParam String ani,@RequestParam String pricePoint,RedirectAttributes redir)
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("Single Subscription for Service {[MUSIC SCORER}], ANI {["+ani+"]} and Pack {["+pricePoint+"]}");
			String response = musicActionDao.musicSingleSubscription(ani,pricePoint);
			redir.addFlashAttribute("msg",response);
			return "redirect:/music_SingleSubscription";
		}
	}
	
	
	
	@RequestMapping(value="music_BulkSubscription")
	public String musicBulkSubscription()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("Bulk Subscription for Service {[MUSIC SCORER}]");
			return "music-bulkSubscription";
		}
	}
	
	
	
	@RequestMapping(value="music_BulkSubscriptionAction")
	public String modBulkSubscriptionAction(@RequestParam String pricePoint,@RequestParam MultipartFile ani,RedirectAttributes redir)
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			String fileName = ani.getOriginalFilename();
			
			if(fileName.substring(fileName.lastIndexOf(".")+1).equalsIgnoreCase("txt"))
			{
				LOGGER.info("Bulk Subscription for Service {[MOD}]");
				copyMetaFile(ani);
				LOGGER.info("File Copied to {["+metaPath+"}]");
				String response = musicActionDao.bulkMusicSinleSubscription(pricePoint,metaPath);
			}
			else
			{
				LOGGER.info("Extension is not txt");
				redir.addFlashAttribute("msg", "Please upload a txt file");
			}
			
			return "redirect:/music_BulkSubscription";
		}
	}
	
	
	@RequestMapping(value="music_SingleUnSubscription")
	public String musicSingleUnSubscription()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("UnSubscription for Service {[MUSIC SCORER}]");
			return "music-singleUnSubscription";
		}
	}
	
	
	@RequestMapping(value="music_UserBillingDetails")
	public String modUserBillingDetails()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("User Billing Deatils for Service {[MUSIC SCORER}]");
			return "music-userBillingDetails";
		}
	}
	
	@RequestMapping(value="music_userBillingDetailsAction",method=RequestMethod.POST)
	public String userBillingDetails(@RequestParam String ani,RedirectAttributes redir)
	{
		List<UserBillingDetails> userBillingDetails = null;
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("UserBilling Details for Service {[MUSIC SCORER}] and ANI {["+ani+"]}");
			userBillingDetails = musicActionDao.getMusicUserBillingDetails(ani);
			
			if(userBillingDetails.isEmpty()==false)
			{
				redir.addFlashAttribute("data",userBillingDetails);
			}
			
			else
			{
				LOGGER.info("No Billing Details for Service {[MUSIC SCORER}] and ANI {["+ani+"]}");
				redir.addFlashAttribute("msg","No User Details Found");
			}
			
			return "redirect:/mod_UserBillingDetails";
		}
	}
	
	
	public String copyMetaFile(MultipartFile metafile) {
		try {
			String filename = metafile.getOriginalFilename().substring(0,
					metafile.getOriginalFilename().lastIndexOf("."));
			String ext = metafile.getOriginalFilename().substring(metafile.getOriginalFilename().lastIndexOf(".") + 1);

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyyHHmm");
			Date date = new Date();

			byte[] bytes = metafile.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
					new File(UPLOAD_DIRECTORY + File.separator + filename + "_" + formatter.format(date) + "." + ext)));
			stream.write(bytes);
			stream.flush();
			stream.close();
			metaPath = UPLOAD_DIRECTORY + File.separator + filename + "_" + formatter.format(date) + "." + ext;
			return metaPath;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}