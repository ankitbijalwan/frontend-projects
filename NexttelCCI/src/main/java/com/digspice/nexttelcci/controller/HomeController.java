package com.digspice.nexttelcci.controller;

import javax.servlet.http.HttpSession;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.digspice.nexttelcci.dao.HomeActoinDao;
import com.digspice.nexttelcci.mobileRadio.entity.UserLoginEntity;


@Controller
public class HomeController {
	
	@Autowired
	HttpSession session;
	
	@Autowired
	HomeActoinDao homeActoinDao; 

	private static final Logger LOGGER = Logger.getLogger(HomeController.class);
	
	@RequestMapping(value="/")
	public String start()
	{
		LOGGER.info("Welcome to Nexttel CCI Portal");
		return "login"; 
	}
	
	
	@RequestMapping(value="/loginAction",method = RequestMethod.POST)
	public String login(@RequestParam String username,@RequestParam String password)
	{
		String viewName=null;
		UserLoginEntity userLoginEntity = homeActoinDao.checkLoginAuthentication(username, password);
		if(userLoginEntity != null)
		{
			LOGGER.info("Successfully login by "+ userLoginEntity.toString());
			session.setAttribute("userObj", userLoginEntity);
			if(userLoginEntity.getAccessArea().equalsIgnoreCase("portal"))
				viewName = "redirect:/home";
			else
				viewName = "mis/music_mis";
		}
		else
		{
				viewName = "login";
		}
		
		return viewName;
	}
	
	
	@RequestMapping(value="/home")
	public String home()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		
		else
		{
			return "home";
		}
	}
	
	@RequestMapping(value="/logout")
	public String logout()
	{
		if(session.getAttribute("userObj")!=null)
		{
			session.removeAttribute("userObj");
			session.invalidate();
		}
		else
		{
			session.invalidate();
		}
		
		LOGGER.info("Logout Sucessfully");
		return "login"; 
	}
	
	
}
