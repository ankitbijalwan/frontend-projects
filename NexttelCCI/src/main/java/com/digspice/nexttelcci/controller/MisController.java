package com.digspice.nexttelcci.controller;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digspice.nexttelcci.daoImpl.MODMisDataExcel;
import com.digspice.nexttelcci.daoImpl.ModMisDataDaoImpl;
import com.digspice.nexttelcci.daoImpl.MusicMisDataDaoImpl;
import com.digspice.nexttelcci.daoImpl.MusicMisDataExcel;
import com.digspice.nexttelcci.mobileRadio.entity.NexttelModMISEntity;
import com.digspice.nexttelcci.music.entity.NexttelMusicMISEntity;

@Controller
public class MisController {
	
	@Autowired
	ModMisDataDaoImpl modMisDataDaoImpl;
	
	@Autowired
	MusicMisDataDaoImpl musicMisDataDaoImpl;
	
	@Autowired
	MusicMisDataExcel musicMisDataExcel;
	
	@Autowired
	MODMisDataExcel modMisDataExcel;
	
	String fromDateText;
	String toDateText;
	
	private static final Logger LOGGER = Logger.getLogger(MisController.class);
	
	@RequestMapping(value="/serviceChange")
	public String serviceChange(@RequestParam String accesspage)
	{
		LOGGER.info(accesspage);
		if(accesspage.equalsIgnoreCase("music_scorrer"))
			return "mis/music_mis";
		else
			return "mis/mod_mis";
	}
	
	
	@RequestMapping(value="/musicMIS",method=RequestMethod.POST)
	public String musicMIS(@RequestParam String fromDate,@RequestParam String toDate,RedirectAttributes redir)
	{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		Date fDate = null;
		Date eDate = null;
		try{
			fDate = df.parse(fromDate);
			eDate = df.parse(toDate);
			
			fromDateText = df1.format(fDate);
			toDateText = df1.format(eDate);
		}
		
		catch(Exception e)
		{
			LOGGER.info("Exception in Conversion of DateFormat "+e);
		}
		
		LOGGER.info("Fetching data for MUSIC Service between date  {["+fromDateText+" "+toDateText+"]} ");
		List<NexttelMusicMISEntity> listNexttelMusicMIS = musicMisDataDaoImpl.getMusicMisData(fromDateText, toDateText);
		
		redir.addFlashAttribute("reportdata", listNexttelMusicMIS);
		redir.addAttribute("accesspage", "music_scorrer");
		return "redirect:/serviceChange";
	}
	
	@RequestMapping(value="/modMIS",method=RequestMethod.POST)
	public String modMIS(@RequestParam String fromDate,@RequestParam String toDate,RedirectAttributes redir)
	{
		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		Date fDate = null;
		Date eDate = null;
		try{
			fDate = df.parse(fromDate);
			eDate = df.parse(toDate);
			
			fromDateText = df1.format(fDate);
			toDateText = df1.format(eDate);
		}
		
		catch(Exception e)
		{
			LOGGER.info("Exception in Conversion of DateFormat "+e);
		}
		
		LOGGER.info("Fetching data for MOD Service between date  {["+fromDateText+" "+toDateText+"]} ");
		List<NexttelModMISEntity> listNexttelModMIS = modMisDataDaoImpl.getMODMisData(fromDateText, toDateText);
		redir.addFlashAttribute("reportdata", listNexttelModMIS);
		redir.addAttribute("accesspage", "mod");
		return "redirect:/serviceChange";
		
	}
	
	
	@RequestMapping(value="/downloadMusicreportxls")
	public String downloadMusicExcel(HttpServletRequest request, HttpServletResponse response,RedirectAttributes redir)
	{
		if(fromDateText!=null && toDateText!=null)
		{
			ClassPathResource resource = null;
			LOGGER.info("Downloadong Excel Report for MUSIC Service");
			List<NexttelMusicMISEntity> listNexttelMusicMIS = musicMisDataDaoImpl.getMusicMisData(fromDateText, toDateText);
			
			if(listNexttelMusicMIS!=null)
			{
				resource = new ClassPathResource("Nexttel_MUSIC_MIS.xlsx");
				InputStream is = null;
				try {
					is = ((InputStreamSource)resource).getInputStream();
				} catch (Throwable e) {
					e.printStackTrace();
				}
				
				int startRowIndex = 0;
				int startColIndex = 3;
				musicMisDataExcel.genrateXLS(is, startRowIndex, startColIndex, response, listNexttelMusicMIS,fromDateText,toDateText);
				
				fromDateText=null;
				toDateText=null;
				
			}
		}
		
		else
		{
			LOGGER.info("Start and End date is not Selected");
		}
		
		redir.addAttribute("accesspage", "music_scorrer");
		return "redirect:/serviceChange";
	}
	
	
	@RequestMapping(value="/downloadMODreportxls")
	public String downloadMODExcel(HttpServletRequest request, HttpServletResponse response,RedirectAttributes redir)
	{
		if(fromDateText!=null && toDateText!=null)
		{
			ClassPathResource resource = null;
			LOGGER.info("Downloadong Excel Report for MOD Service");
			List<NexttelModMISEntity> listNexttelMODMIS = modMisDataDaoImpl.getMODMisData(fromDateText, toDateText);
			
			if(listNexttelMODMIS!=null)
			{
				resource = new ClassPathResource("Nexttel_MOD_MIS.xlsx");
				InputStream is = null;
				try {
					is = ((InputStreamSource)resource).getInputStream();
				} catch (Throwable e) {
					e.printStackTrace();
				}
				
				int startRowIndex = 0;
				int startColIndex = 3;
				modMisDataExcel.genrateXLS(is, startRowIndex, startColIndex, response, listNexttelMODMIS,fromDateText,toDateText);
				
				fromDateText=null;
				toDateText=null;
			}
			
		}
		
		else
		{
			LOGGER.info("Start and End date is not Selected");
			
		}
		
		redir.addAttribute("accesspage", "mod");
		return "redirect:/serviceChange";
	}
	

}
