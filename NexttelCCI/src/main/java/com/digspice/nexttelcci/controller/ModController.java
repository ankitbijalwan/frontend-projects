package com.digspice.nexttelcci.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digspice.nexttelcci.dao.ModActionDao;
import com.digspice.nexttelcci.mobileRadio.entity.ModSubDetails;
import com.digspice.nexttelcci.mobileRadio.entity.ModUnsubDetail;
import com.digspice.nexttelcci.mobileRadio.entity.UserBillingDetails;

@Controller
public class ModController {
	
	@Autowired
	HttpSession session;
	
	@Autowired
	ModActionDao modActionDao;
	
	
	private static final Logger LOGGER = Logger.getLogger(ModController.class);
	
	private static final String UPLOAD_DIRECTORY = "E:\\NexttelMod\\Bulk_MUSIC_Files\\";
	String metaPath = null;
	
	@RequestMapping(value="/NeXtMusic/checkUserDetails")
	public String home()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		
		else
		{
			return "mod-checkUserDetails";
		}
	}
	
	@RequestMapping(value="userDetailsAction",method=RequestMethod.POST)
	public String userDetailsAction(@RequestParam String detail_type,@RequestParam String ani,RedirectAttributes redir)
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("Fetching user detail for Service {[MOD}] ANI {["+ani+"]} for type {["+detail_type+"]}");
			if(detail_type.equalsIgnoreCase("subscription"))
			{
				ArrayList<ModSubDetails> subdetail= modActionDao.modUserDetailsforsub(ani);
				LOGGER.info("No. of records in Subcription table is {["+subdetail.size()+"]}");
				if(subdetail.isEmpty()==false)
				{
					redir.addFlashAttribute("subdata", subdetail);
				}
				else
				{
					redir.addFlashAttribute("msg", "No subscription Detail found for "+ani);
				}
			}
			else if(detail_type.equalsIgnoreCase("un-subscription"))
			{
				ArrayList<ModUnsubDetail> unsubdetail= modActionDao.modUserDetailsforUnsub(ani);
				LOGGER.info("No. of records in UnSubcription table is {["+unsubdetail.size()+"]}");
				if(unsubdetail.isEmpty()==false)
				{
					redir.addFlashAttribute("unsubdata", unsubdetail);
				}
				else
				{
					redir.addFlashAttribute("msg", "No Unsubscription Detail found for "+ani);
				}
			}
			else if(detail_type.equalsIgnoreCase("download"))
			{

			}
			else if(detail_type.equalsIgnoreCase("dedication"))
			{
				
			}
				
			else if(detail_type.equalsIgnoreCase("playlist"))
			{
			
			}
			
			return "redirect:/NeXtMusic/checkUserDetails";
		}
	}
	
	
	@RequestMapping(value="mod_SingleSubscription")
	public String modSingleSubscription()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("Single Subscription for Service {[MOD}]");
			return "mod-singleSubscription";
		}
	}
	
	
	@RequestMapping(value="mod_SingleSubscriptionAction")
	public String modSingleSubscriptionAction(@RequestParam String ani,@RequestParam String pricePoint,RedirectAttributes redir)
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("Single Subscription for Service {[MOD}], ANI {["+ani+"]} and Pack {["+pricePoint+"]}");
			String response = modActionDao.modSinleSubscription(ani,pricePoint);
			redir.addFlashAttribute("msg",response);
			return "redirect:/mod_SingleSubscription";
		}
	}
	
	
	@RequestMapping(value="mod_BulkSubscription")
	public String modBulkSubscription()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			return "mod-bulkSubscription";
		}
	}
	
	
	@RequestMapping(value="mod_BulkSubscriptionAction")
	public String modBulkSubscriptionAction(@RequestParam String pricePoint,@RequestParam MultipartFile ani,RedirectAttributes redir)
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			String fileName = ani.getOriginalFilename();
			
			if(fileName.substring(fileName.lastIndexOf(".")+1).equalsIgnoreCase("txt"))
			{
				LOGGER.info("Bulk Subscription for Service {[MOD}]");
				copyMetaFile(ani);
				LOGGER.info("File Copied to {["+metaPath+"}]");
				String response = modActionDao.bulkModSinleSubscription(pricePoint,metaPath);
				if(response.equalsIgnoreCase("success"))
				{
					redir.addFlashAttribute("msg", "Bulk Subscription is Successfull");
				}
				else if(response.equalsIgnoreCase("exception"))
				{
					redir.addFlashAttribute("msg", "Something went wrong!!! Please contact to your Administrator");
				}
			}
			else
			{
				LOGGER.info("Extension is not txt");
				redir.addFlashAttribute("msg", "Please upload a .txt file");
			}
			
			return "redirect:/mod_BulkSubscription";
		}
	}
	
	
	@RequestMapping(value="mod_SingleUnSubscription")
	public String modSingleUnSubscription()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("UnSubscription for Service {[MOD}]");
			return "mod-singleUnSubscription";
		}
	}
	
	
	@RequestMapping(value="mod_UnSubscriptionAction")
	public String modUnSubscriptionAction(@RequestParam String ani,RedirectAttributes redir)
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("UnSubscription from Service {[MOD}] for ANI {["+ani+"]}");
			String response = modActionDao.modUnSubscription(ani);
			if(response == "1")
			{
				redir.addFlashAttribute("msg","User Successfully Unsubscribe");
			}
			
			else if(response=="0")
			{
				redir.addFlashAttribute("msg","User not Successfully Unsubscribe");
			}
			
			else
			{
				redir.addFlashAttribute("msg", "Something went wrong!!! Please contact to your Administrator");
			}
			
			return "redirect:/mod_SingleUnSubscription";
		}
	}
	
	@RequestMapping(value="mod_UserBillingDetails")
	public String modUserBillingDetails()
	{
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("User Billing Deatils for Service {[MOD}]");
			return "mod-userBillingDetails";
		}
	}
	
	
	@RequestMapping(value="mod_userBillingDetailsAction",method=RequestMethod.POST)
	public String userBillingDetails(@RequestParam String ani,RedirectAttributes redir)
	{
		List<UserBillingDetails> userBillingDetails = null;
		if(session.getAttribute("userObj")==null)
		{
			return "redirect:/logout";
		}
		else
		{
			LOGGER.info("UserBilling Details for Service {[MOD}] and ANI {["+ani+"]}");
			userBillingDetails = modActionDao.getModUserBillingDetails(ani);
			
			if(userBillingDetails.isEmpty()==false)
			{
				redir.addFlashAttribute("data",userBillingDetails);
			}
			
			else
			{
				LOGGER.info("No Billing Details for Service {[MOD}] and ANI {["+ani+"]}");
				redir.addFlashAttribute("msg","No User Details Found");
			}
			
			return "redirect:/mod_UserBillingDetails";
		}
	}
	
	
	

	public String copyMetaFile(MultipartFile metafile) {
		try {
			LOGGER.info("File name is "+metafile.getOriginalFilename());
			String filename = metafile.getOriginalFilename().substring(0,
					metafile.getOriginalFilename().lastIndexOf("."));
			LOGGER.info("Uploaded File name for Bulk Subscription is "+filename);
			String ext = metafile.getOriginalFilename().substring(metafile.getOriginalFilename().lastIndexOf(".") + 1);
			LOGGER.info("Uploaded File Extension for Bulk Subscription is "+ext);
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyyHHmm");
			Date date = new Date();

			byte[] bytes = metafile.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
					new File(UPLOAD_DIRECTORY + File.separator + filename + "_" + formatter.format(date) + "." + ext)));
			stream.write(bytes);
			stream.flush();
			stream.close();
			metaPath = UPLOAD_DIRECTORY + File.separator + filename + "_" + formatter.format(date) + "." + ext;
			LOGGER.info("uploaded File Path is " +metaPath);
			return metaPath;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
