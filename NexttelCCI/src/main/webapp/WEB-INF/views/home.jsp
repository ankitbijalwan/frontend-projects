<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Nexttel neXtMusic</title>
<meta name="viewport" content="width=device-width,initial-scale=1">

<!-- basic css -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
</head>

<body>
<jsp:include page="header.jsp"></jsp:include>

<!-- Page Content-->
<div class="wrapper">
  <div class="container-fluid">
  <div>&nbsp;</div>
    
    
    
    
    <!-- end row -->
        <div style="padding:15px; width:550px; margin:0px auto;  background-color:#FFF;  padding:15px; margin-bottom:20px;">
    <div style="  border: 1px dotted; padding:10px;">
<div align="center" style="text-align:center; margin-top:10px;margin-bottom:10px;padding-bottom:20px; font-weight:bold;font-size:17px;"><span style="background-color:#e7eff7;padding:8px;">neXtMusic-User Details</span></div>
                <div class="p-20">
                  <form action="userDetailsAction" method="post" onsubmit="return validate()">
                    <div class="form-group" style="display:flex;">
                      <label style="width:70%;">Enter Mobile Number<span class="sta-red">*</span></label>
                      <input type="text" placeholder="Please enter 9 digit ANI"  name="ani" id="ani" class="form-control" required="required" autocomplete="off">
                      <span class="font-13 text-muted"></span></div>
                      
                     <div class="form-group" style="display:flex;">
                      <label style="width:70%;">Detail Type<span class="sta-red">*</span></label>
                      <select name="detail_type" id="detail_type" class="form-control" required="required">
                      <option selected="selected" value="-1">Select Option</option>
                      <option value="subscription">Subscription</option>
                      <option value="un-subscription">Un-Subscription</option>
                      <!-- <option value="download">Download</option>
                      <option value="dedication">Dedication</option>
                      <option value="playlist">My Play List</option> -->
                      </select>
                      <span class="font-13 text-muted"></span></div>
                      
                    <div class="form-group" style="display:flex; padding-left:40%;">
                    
                     <button class="btn btn-light mb-0" type="submit">Submit </button>
                     </div>
                    
                    
                    
                  </form>
                </div>
              </div></div>
              
            </div></div>
    
    
    
    
    
    <div class="row" style="padding-left:10px;padding-right:10px;">
      <div class="col-lg-12">
      
      
      <c:if test="${fn:length(subdata) gt 0}">
      <p style="text-align:center;font-size:16px;font-weight:bold;">User Details-Subscription</p>
        <div class="card">
          <div class="card-body">
          
          
          
            
      <table id="sub" class="display nowrap" style="width:100%">
                <thead>
                  <tr style="text-align: center;">
                  	<th>Mobile Number</th>
                    <th>Language</th>
                    <th>Status</th>
                    <th>Subscription Date</th>
                    <th>Renew Date</th>
                    <th>Billing Date</th>
                    <th>Pack</th>
                    <th>Amount</th>
                    <th>Activation Source</th>
                  </tr>
                </thead>
                <tbody>
                	
               <c:forEach var="open" items="${subdata}">                
                <tr style="text-align: center;">
                	<td>${open.MOBILE_NUMBER}</td>
					<td>${open.LANGUAGE}</td>
					<td>${open.STATUS}</td>
                	<td>${open.SUB_DATE}</td>
                	<td>${open.RENEW_DATE}</td>
                	<td>${open.BILLING_DATE}</td>
                	<td>${open.PACK_TYPE}</td>
                	<td>${open.AMOUNT}</td>
                	<td>${open.ACT_SOURCE}</td>
                  </tr>
                </c:forEach>
                </tbody>
              </table>
              
              </div>
              </div>
              </c:if>
              <div>
              
              
              </div>
              <c:if test="${fn:length(unsubdata) gt 0}">
              <p style="text-align:center;font-size:16px;font-weight:bold;">User Details-UnSubscription</p>
              <div class="card">
          <div class="card-body">
              
                    <table id="unsub" class="display nowrap" style="width:100%">
                <thead>
                  <tr style="text-align: center;">
                  	<th>Mobile Number</th>
                    <th>Language</th>
                    <th>Subscription Date</th>
                    <th>Renew Date</th>
                    <th>Billing Date</th>
                    <th>Un-Subscription Date</th>
                    <th>Pack</th>
                    <th>Amount</th>
                    <th>Activation Source</th>
                    <th>De-Activation Source</th>
                  </tr>
                </thead>
                <tbody>
                	
                <c:forEach var="open" items="${unsubdata}">
                <tr style="text-align: center;">
                	<td>${open.MOBILE_NUMBER}</td>
                	<td>${open.LANGUAGE}</td>
                	<td>${open.SUB_DATE}</td>
                	<td>${open.RENEW_DATE}</td>
                	<td>${open.BILLING_DATE}</td>
                	<td>${open.UNSUB_DATE}</td>
                	<td>${open.PACK_TYPE}</td>
                	<td>${open.AMOUNT}</td>
                	<td>${open.ACT_SOURCE}</td>
               		<td>${open.UNSUB_MODE}</td>  	                	
                 </tr>
                 </c:forEach>
                </tbody>
              </table>
              
            </div>
          </div>
          </c:if>
        </div>
      </div>
  <!-- end container-fluid -->
<!-- end page-wrapper -->

<jsp:include page="footer.jsp"></jsp:include>

<!-- jQuery  -->

<script type="text/javascript" src="resources/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="resources/js/jquery.dataTables.min.js"></script>

<!-- <script src="resources/js/jquery.min.js"></script> -->
<script src="resources/js/bootstrap.bundle.min.js"></script>
<script src="resources/js/jquery.slimscroll.min.js"></script>

<script type="text/javascript" src="resources/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="resources/js/form-advanced.js"></script>


<!-- Buttons examples --><!-- Responsive examples -->
<script type="text/javascript" src="resources/js/bootbox.js"></script>

<input type="hidden" id="snackbar" value="${msg}">

<script type="text/javascript">
$(document).ready(function() {
    $('#sub').DataTable( {
    	"scrollX": true,
    	scrollY:        '40vh',
        scrollCollapse: true,
        paging:         false
    } );
} );
</script>


<script type="text/javascript">
$(document).ready(function() {
    $('#unsub').DataTable( {
    	"scrollX": true,
    	scrollY:        '40vh',
        scrollCollapse: true,
        paging:         false
    } );
} );
</script>

<script type="text/javascript">
$(document).ready(function(){
	
	var x= document.getElementById("snackbar").value;
	
		bootbox.alert({
		message: x, 
		callback: function(){}
		});
	
		
	
});
</script>
<script type="text/javascript">
function validate()
{
	var pattern= /^[0-9]+$/;
	
	if(!pattern.test($('#ani').val()))
	{
		alert("Please Insert only Digits");
		$(this).focus();
		return false;
	}
	
	else if($('#ani').val().length != 9)
		{
			alert("Number length should be 9");
			return false;
		}
	else if($('#detail_type').val() == '-1')
		{
			alert("Please Select Detail Type");
			return false;
		}
	else
		{
			return true;
		}
	}

</script>
</body>
</html>