<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Nexttel neXtMusic</title>
<meta name="viewport" content="width=device-width,initial-scale=1">



<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
</head>
<body class="loginbg1">
<!-- Log In page -->
<div class="wrapper-page" >
  <div class="card">
    <div class="card-body">
      <div class="p-3">
        <div class="float-right text-right">
          <h4 class="font-18 mt-3 m-b-5">Welcome !</h4>
          <p class="text-muted">Sign in to Nexttel Portal</p>
        </div>
        <a href="index.html" class="logo-admin"><img src="resources/images/logo.png" width="150" height="46"></a></div>
      <div class="p-3">
        <form class="form-horizontal m-t-10" action="loginAction" method="post">
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" placeholder="Enter username" name="username" required="required" autocomplete="off">
          </div>
          <div class="form-group">
            <label >Password</label>
            <input type="password" class="form-control"  placeholder="Enter password" name="password" required="required" autocomplete="off">
          </div>
          <div class="form-group row m-t-30">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6 text-right">
              <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
            </div>
          </div>
          <div class="form-group m-t-30 mb-0 row">
            
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="m-t-40 text-center text-white-50">
   
   
  </div>
</div>

</body>

</html>