<!-- header start here -->
<div class="topbar">
  <div class="topbar-main">
    <div class="container-fluid"><!-- LOGO -->
      <div class="topbar-left"><a href="index.html" class="logo"><span style="color:#FFF;"><img src="resources/images/logo.png" class="logor"  width="253" height="29"></span></a></div>
      <!-- Navbar -->
      <nav class="navbar-custom"><!-- Search input -->
        <div class="search-wrap" id="search-wrap">
          <div class="search-bar">
            <input class="search-input" type="search" placeholder="Search here..">
            <a href="javascript:void(0);" class="close-search search-btn" data-target="#search-wrap"><i class="mdi mdi-close-circle"></i></a></div>
        </div>
        <ul class="list-unstyled topbar-nav float-right mb-0">
          
          
          
          
          <li class="dropdown"><a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="fa fa-user-circle-o" style="font-size:28px;color:#fff; padding-top:22px;"></i> <span class="ml-1 nav-user-name hidden-sm" style="vertical-align:7px;">${userObj.username}<i class="mdi mdi-chevron-down"></i></span></a>
            <div class="dropdown-menu dropdown-menu-right">   
              
              <a class="dropdown-item" href="logout"><i class="dripicons-exit text-muted mr-2"></i> Logout</a></div>
          </li>
          <li class="menu-item"><!-- Mobile menu toggle--> <a class="navbar-toggle nav-link" id="mobileToggle">
            <div class="lines"><span></span> <span></span> <span></span></div>
            </a></li>
        </ul>
        <ul class="list-unstyled topbar-nav mb-0">
          
          
        </ul>
      </nav>
      </div>
  </div>
  <!-- MENU Start -->
  <div class="navbar-custom-menu">
    <div class="container-fluid">
      <div id="navigation"><!-- Navigation Menu-->
        <ul class="navigation-menu">
          <li class="has-submenu"><a href="#"><i class="mdi mdi-email"></i>neXtMusic</a>
            <ul class="submenu">
              <li><a href="home">User Details</a></li>
              <li><a href="mod_UserBillingDetails">User Billing Details</a></li>
              <li><a href="mod_SingleSubscription">Single Subscription</a></li>
              <li><a href="mod_BulkSubscription">Bulk Subscription</a></li>
              <li><a href="mod_SingleUnSubscription">Single Un-Subscription</a></li>
            </ul> 
          </li>
          <!-- <li class="has-submenu"><a href="#"><i class="mdi mdi-music"></i>Music Scorer</a>
            <ul class="submenu">
              <li><a href="music_UserDetails">User Details</a></li>
              <li><a href="music_UserBillingDetails">User Billing Details</a></li>
              <li><a href="music_SingleSubscription">Single Subscription</a></li>
              <li><a href="music_BulkSubscription">Bulk Subscription</a></li>
              <li><a href="music_SingleUnSubscription">Single Un-Subscription</a></li>
            </ul> 
          </li> -->          
          
			  
        </ul>
      </div>
     </div>
   </div>
  </div>
<!-- header end here -->