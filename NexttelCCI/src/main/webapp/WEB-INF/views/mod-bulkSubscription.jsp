<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Nexttel neXtMusic</title>
<meta name="viewport" content="width=device-width,initial-scale=1">

<!-- basic css -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="resources/css/icons.css" rel="stylesheet" type="text/css">
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
</head>

<body>
<jsp:include page="header.jsp"></jsp:include>

<!-- Page Content-->
<div class="wrapper">
  <div class="container-fluid">
  <div>&nbsp;</div>
    
    
    
    
    <!-- end row -->
        <div style="padding:15px; width:550px; margin:0px auto;  background-color:#FFF;  padding:15px; margin-bottom:20px;">
    <div style="  border: 1px dotted; padding:10px;">
<div align="center" style="text-align:center; margin-top:10px;margin-bottom:10px;padding-bottom:20px; font-weight:bold;font-size:17px;"><span style="background-color:#e7eff7;padding:8px;">neXtMusic-Single Subscription</span></div>
                <div class="p-20">
                  <form action="mod_BulkSubscriptionAction" method="post" enctype="multipart/form-data" onsubmit="return validate()">	
                    <div class="form-group" style="display:flex;">
                      <label style="width:70%;">Bulk File<span class="sta-red">*</span></label>
                      <input type="file" name="ani" id="ani" class="form-control" required="required">
                      <span class="font-13 text-muted"></span>
                      </div>
                      
                      
                      <div class="form-group" style="display:flex;">
                      <label style="width:70%;">Select Price Point<span class="sta-red">*</span></label>
                      <select class="form-control" id="pricePoint" name="pricePoint" required="required">
                      <option value="-1">Select Price Point</option>
                      <option value="MR_SUB1">Monthly</option>
                      <option value="MR_SUB2">Weekly</option>
                      <option value="MR_SUB3">Daily</option>
                      </select>
                      </div>
                    <div class="form-group" style="display:flex; padding-left:40%;">
                    
                     <button class="btn btn-light mb-0" type="submit">Submit </button>
                     </div>
                    
                    
                    
                  </form>
                </div>
              </div></div>
              
            </div></div>
    
    
    
    
    
      <!-- end container-fluid -->
<!-- end page-wrapper -->

<jsp:include page="footer.jsp"></jsp:include>

<!-- jQuery  -->

<script type="text/javascript" src="resources/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="resources/js/jquery.dataTables.min.js"></script>

<!-- <script src="resources/js/jquery.min.js"></script> -->
<script src="resources/js/bootstrap.bundle.min.js"></script>
<script src="resources/js/jquery.slimscroll.min.js"></script>

<script type="text/javascript" src="resources/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="resources/js/form-advanced.js"></script>


<!-- Buttons examples --><!-- Responsive examples -->
<script type="text/javascript" src="resources/js/bootbox.js"></script>

<input type="hidden" id="snackbar" value="${msg}">

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable( {
    	"scrollX": true,
    	scrollY:        '40vh',
        scrollCollapse: true,
        paging:         false
    } );
} );
</script>

<script type="text/javascript">
$(document).ready(function(){
	
	var x= document.getElementById("snackbar").value;
	
		bootbox.alert({
		message: x, 
		callback: function(){}
		});
	
		
	
});
</script>
<script type="text/javascript">
function validate()
{
	if($('#pricePoint').val() == '-1')
		{
			alert("Please Select Price Point");
			$(this).focus();
			return false;
		}
	
	else
		{
			return true;
		}
	
	}

</script>
</body>


</html>