package com.digispice.cci.zambia.mispoi;

import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.digispice.cci.zambia.entity.ZamtelGamesMISEntity;

@Component
public class ZemtalGamesPOI {

	private static Logger logger = LoggerFactory.getLogger(ZemtalGamesPOI.class);

	private void fillReport(Sheet worksheet, int startRowIndex, int startColIndex, List<ZamtelGamesMISEntity> bean,
			XSSFCellStyle my_style) {

		try {
			DecimalFormat df = new DecimalFormat("#.####");
			df.setRoundingMode(RoundingMode.CEILING);

			Row rowHeader = null;
			Cell cell = null;
			int rowCount = startRowIndex;
			int colcount = startColIndex;

			for (ZamtelGamesMISEntity data : bean) {

				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				cell.setCellValue(data.getsDate());

				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);

				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue("Total");
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			float totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				float a = 0;
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);

				if (data.getTotalBase() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalBase()));
				}
				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getActiveBase() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getActiveBase()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {

				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalSubscriptionRequestsReceived() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalSubscriptionRequestsReceived()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);

			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);

				float a = 0;
				if (data.getTotalSecondConsentReceived() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalSecondConsentReceived()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalRequestProcessWAP() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalRequestProcessWAP()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalRequestProcessSMS() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalRequestProcessSMS()));
				}

				totalValue = totalValue + a;

				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);

				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalRequestProcessUSSD() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalRequestProcessUSSD()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalSubscriptionRequestsProcessed() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalSubscriptionRequestsProcessed()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);

			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalGrossAdds() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalGrossAdds()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getVoluntaryChurn() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getVoluntaryChurn()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getWapMode() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getWapMode()));
				}

				totalValue = totalValue + a;

				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);

			startColIndex = colcount;
			totalValue = 0;
			rowCount++;
			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getSmsMode() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getSmsMode()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getUssdMode() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getUssdMode()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getInvoluntaryChurn() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getInvoluntaryChurn()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalChurn() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalChurn()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getNetAdds() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getNetAdds()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalRevenueInZK() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalRevenueInZK()));
				}

				totalValue = totalValue + a;

				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getSubscriptionRevenue() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getSubscriptionRevenue()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getResubscriptionRevenue() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getResubscriptionRevenue()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);

				float a = 0;
				if (data.getSubRevenueFromSMS() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getSubRevenueFromSMS()));
				}
				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getSubRevenueFromWAP() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getSubRevenueFromWAP()));
				}
				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getSubRevenueFromUSSD() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getSubRevenueFromUSSD()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalPageViews() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalPageViews()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalVisits() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalVisits()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getUniqueVisitors() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getUniqueVisitors()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getDailySubscription() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getDailySubscription()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getWeeklySubscription() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getWeeklySubscription()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getMonthlySubscription() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getMonthlySubscription()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getDailyReSubscription() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getDailyReSubscription()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getWeeklyReSubscription() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getWeeklyReSubscription()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getMonthlyReSubscription() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getMonthlyReSubscription()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalCategoryWiseDownloadRequest() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalCategoryWiseDownloadRequest()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getAndroidGamesRequest() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getAndroidGamesRequest()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getHTML5GamesRequest() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getHTML5GamesRequest()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getTotalCategoryWiseDownloadSuccess() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getTotalCategoryWiseDownloadSuccess()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getAndroidGamesSuccess() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getAndroidGamesSuccess()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);
			startColIndex = colcount;
			totalValue = 0;
			rowCount++;

			for (ZamtelGamesMISEntity data : bean) {
				rowHeader = worksheet.getRow((short) rowCount);
				if (rowHeader == null)
					rowHeader = worksheet.createRow((short) rowCount);
				rowHeader.setHeight((short) 270);

				cell = rowHeader.getCell(startColIndex);
				if (cell == null)
					cell = rowHeader.createCell(startColIndex);
				float a = 0;
				if (data.getHtml5GamesSuccess() == null) {
					a = 0;
				} else {
					a = (Float.parseFloat(data.getHtml5GamesSuccess()));
				}

				totalValue = totalValue + a;
				cell.setCellValue(a);
				cell.setCellStyle(my_style);
				cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
				cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
				cell.getCellStyle().setBorderRight(BorderStyle.THIN);
				cell.getCellStyle().setBorderTop(BorderStyle.THIN);
				startColIndex++;
			}

			rowHeader = worksheet.getRow((short) rowCount);
			if (rowHeader == null)
				rowHeader = worksheet.createRow((short) rowCount);
			rowHeader.setHeight((short) 270);

			cell = rowHeader.getCell(startColIndex);
			if (cell == null)
				cell = rowHeader.createCell(startColIndex);
			cell.setCellValue(totalValue);
			cell.setCellStyle(my_style);
			cell.getCellStyle().setBorderLeft(BorderStyle.THIN);
			cell.getCellStyle().setBorderBottom(BorderStyle.THIN);
			cell.getCellStyle().setBorderRight(BorderStyle.THIN);
			cell.getCellStyle().setBorderTop(BorderStyle.THIN);

		} catch (Exception e) {
			logger.error("Error While Genrating XLSX FIle " + e, e);
		}
	}

	public void genrateZemtalGamesXLS(InputStream is, int startRowIndex, int startColIndex,
			HttpServletResponse response, List<ZamtelGamesMISEntity> bean, String fromdate, String todate) {
		XSSFWorkbook workbook = null;
		try {

			workbook = new XSSFWorkbook(is);

			Sheet worksheet = workbook.getSheetAt(0);

			XSSFCellStyle my_style = workbook.createCellStyle();

			/* Create HSSFFont object from the workbook */
			XSSFFont my_font = workbook.createFont();

			/* attach the font to the style created earlier */
			my_style.setFont(my_font);

			fillReport(worksheet, startRowIndex, startColIndex, bean, my_style);

			// Set Response Header
			response.setHeader("Content-Disposition", "inline; filename=MIS_Zemtal_Games.xlsx");
			response.setContentType("application/vnd.ms-excel");

			ServletOutputStream outputStream = response.getOutputStream();

			// Write to the output stream
			worksheet.getWorkbook().write(outputStream);
			// Flush the stream
			outputStream.flush();
		} catch (Exception e) {
			logger.error("Error while executing GenrateXLS FIle" + e, e);
		}

		finally {
			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String setMonth(String month) {
		if (month.equals("01")) {
			month = "Jan";
			return month;
		} else if (month.equals("02")) {
			month = "Feb";
			return month;
		}

		else if (month.equals("03")) {
			month = "Mar";
			return month;
		} else if (month.equals("04")) {
			month = "Apr";
			return month;
		} else if (month.equals("05")) {
			month = "May";
			return month;
		} else if (month.equals("06")) {
			month = "Jun";
			return month;
		} else if (month.equals("07")) {
			month = "Jul";
			return month;
		} else if (month.equals("08")) {
			month = "Aug";
			return month;
		} else if (month.equals("09")) {
			month = "Sept";
			return month;
		} else if (month.equals("10")) {
			month = "Oct";
			return month;
		} else if (month.equals("11")) {
			month = "Nov";
			return month;
		} else {
			month = "Dec";
			return month;
		}
	}
}