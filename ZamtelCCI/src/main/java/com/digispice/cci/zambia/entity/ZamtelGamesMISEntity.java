package com.digispice.cci.zambia.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="tbl_zemtal_gamhub_dailymis")
public class ZamtelGamesMISEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="date")
	private Date date;
	
	@Transient
	private String sDate;
	
	@Column(name="total_base")
	private String totalBase;
	
	@Column(name="Active_base")
	private String activeBase;
	
	@Column(name="Total_Subscription_Requests_Received")
	private String totalSubscriptionRequestsReceived;
	
	@Column(name="Total_second_consent_received")
	private String totalSecondConsentReceived;
	
	@Column(name="Total_Request_Process_WAP")
	private String totalRequestProcessWAP;
	
	@Column(name="Total_Request_Process_SMS")
	private String totalRequestProcessSMS;
	
	@Column(name="Total_Request_Process_USSD")
	private String totalRequestProcessUSSD;
	
	@Column(name="Total_Subscription_Requests_processed")
	private String totalSubscriptionRequestsProcessed;
	
	@Column(name="Total_Gross_Adds")
	private String totalGrossAdds;
	
	@Column(name="Voluntary_Churn")
	private String voluntaryChurn;
	
	@Column(name="WAP_Mode")
	private String wapMode;
	
	@Column(name="SMS_Mode")
	private String smsMode;
	
	@Column(name="USSD_Mode")
	private String ussdMode;
	
	@Column(name="Involuntary_Churn")
	private String involuntaryChurn;
	
	@Column(name="Total_Churn")
	private String totalChurn;
	
	@Column(name="Net_Adds")
	private String netAdds;
	
	@Column(name="Total_Revenue_in_ZK")
	private String totalRevenueInZK;
	
	@Column(name="Subscription_Revenue")
	private String subscriptionRevenue;
	
	@Column(name="Resubscription_Revenue")
	private String resubscriptionRevenue;
	
	@Column(name="Sub_Revenue_from_SMS")
	private String subRevenueFromSMS;
	
	@Column(name="Sub_Revenue_from_WAP")
	private String subRevenueFromWAP;
	
	@Column(name="Sub_Revenue_from_USSD")
	private String subRevenueFromUSSD;
	
	@Column(name="Total_Page_Views")
	private String totalPageViews;
	
	@Column(name="Total_Visits")
	private String totalVisits;
	
	@Column(name="Unique_Visitors")
	private String uniqueVisitors;
	
	@Column(name="Daily_Subscription")
	private String dailySubscription;
	
	@Column(name="Weekly_Subscription")
	private String weeklySubscription;
	
	@Column(name="Monthly_Subscription")
	private String monthlySubscription;
	
	@Column(name="Daily_ReSubscription")
	private String dailyReSubscription;
	
	@Column(name="Weekly_ReSubscription")
	private String weeklyReSubscription;
	
	@Column(name="Monthly_ReSubscription")
	private String monthlyReSubscription;
	
	@Column(name="Total_Category_Wise_Download_Request")
	private String totalCategoryWiseDownloadRequest;
	
	@Column(name="Android_Games_Request")
	private String androidGamesRequest;
	
	@Column(name="HTML5_Games_Request")
	private String HTML5GamesRequest;
	
	@Column(name="Total_Category_Wise_Download_Success")
	private String totalCategoryWiseDownloadSuccess;
	
	@Column(name="Android_Games_Success")
	private String androidGamesSuccess;
	
	@Column(name="HTML5_Games_Success")
	private String html5GamesSuccess;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTotalBase() {
		return totalBase;
	}

	public void setTotalBase(String totalBase) {
		this.totalBase = totalBase;
	}

	public String getActiveBase() {
		return activeBase;
	}

	public void setActiveBase(String activeBase) {
		this.activeBase = activeBase;
	}

	public String getTotalSubscriptionRequestsReceived() {
		return totalSubscriptionRequestsReceived;
	}

	public void setTotalSubscriptionRequestsReceived(String totalSubscriptionRequestsReceived) {
		this.totalSubscriptionRequestsReceived = totalSubscriptionRequestsReceived;
	}

	public String getTotalSecondConsentReceived() {
		return totalSecondConsentReceived;
	}

	public void setTotalSecondConsentReceived(String totalSecondConsentReceived) {
		this.totalSecondConsentReceived = totalSecondConsentReceived;
	}

	public String getTotalRequestProcessWAP() {
		return totalRequestProcessWAP;
	}

	public void setTotalRequestProcessWAP(String totalRequestProcessWAP) {
		this.totalRequestProcessWAP = totalRequestProcessWAP;
	}

	public String getTotalRequestProcessSMS() {
		return totalRequestProcessSMS;
	}

	public void setTotalRequestProcessSMS(String totalRequestProcessSMS) {
		this.totalRequestProcessSMS = totalRequestProcessSMS;
	}

	public String getTotalRequestProcessUSSD() {
		return totalRequestProcessUSSD;
	}

	public void setTotalRequestProcessUSSD(String totalRequestProcessUSSD) {
		this.totalRequestProcessUSSD = totalRequestProcessUSSD;
	}

	public String getTotalSubscriptionRequestsProcessed() {
		return totalSubscriptionRequestsProcessed;
	}

	public void setTotalSubscriptionRequestsProcessed(String totalSubscriptionRequestsProcessed) {
		this.totalSubscriptionRequestsProcessed = totalSubscriptionRequestsProcessed;
	}

	public String getTotalGrossAdds() {
		return totalGrossAdds;
	}

	public void setTotalGrossAdds(String totalGrossAdds) {
		this.totalGrossAdds = totalGrossAdds;
	}

	public String getVoluntaryChurn() {
		return voluntaryChurn;
	}

	public void setVoluntaryChurn(String voluntaryChurn) {
		this.voluntaryChurn = voluntaryChurn;
	}

	public String getWapMode() {
		return wapMode;
	}

	public void setWapMode(String wapMode) {
		this.wapMode = wapMode;
	}

	public String getSmsMode() {
		return smsMode;
	}

	public void setSmsMode(String smsMode) {
		this.smsMode = smsMode;
	}

	public String getUssdMode() {
		return ussdMode;
	}

	public void setUssdMode(String ussdMode) {
		this.ussdMode = ussdMode;
	}

	public String getInvoluntaryChurn() {
		return involuntaryChurn;
	}

	public void setInvoluntaryChurn(String involuntaryChurn) {
		this.involuntaryChurn = involuntaryChurn;
	}

	public String getTotalChurn() {
		return totalChurn;
	}

	public void setTotalChurn(String totalChurn) {
		this.totalChurn = totalChurn;
	}

	public String getNetAdds() {
		return netAdds;
	}

	public void setNetAdds(String netAdds) {
		this.netAdds = netAdds;
	}

	public String getTotalRevenueInZK() {
		return totalRevenueInZK;
	}

	public void setTotalRevenueInZK(String totalRevenueInZK) {
		this.totalRevenueInZK = totalRevenueInZK;
	}

	public String getSubscriptionRevenue() {
		return subscriptionRevenue;
	}

	public void setSubscriptionRevenue(String subscriptionRevenue) {
		this.subscriptionRevenue = subscriptionRevenue;
	}

	public String getResubscriptionRevenue() {
		return resubscriptionRevenue;
	}

	public void setResubscriptionRevenue(String resubscriptionRevenue) {
		this.resubscriptionRevenue = resubscriptionRevenue;
	}

	public String getSubRevenueFromSMS() {
		return subRevenueFromSMS;
	}

	public void setSubRevenueFromSMS(String subRevenueFromSMS) {
		this.subRevenueFromSMS = subRevenueFromSMS;
	}

	public String getSubRevenueFromWAP() {
		return subRevenueFromWAP;
	}

	public void setSubRevenueFromWAP(String subRevenueFromWAP) {
		this.subRevenueFromWAP = subRevenueFromWAP;
	}

	public String getSubRevenueFromUSSD() {
		return subRevenueFromUSSD;
	}

	public void setSubRevenueFromUSSD(String subRevenueFromUSSD) {
		this.subRevenueFromUSSD = subRevenueFromUSSD;
	}

	public String getTotalPageViews() {
		return totalPageViews;
	}

	public void setTotalPageViews(String totalPageViews) {
		this.totalPageViews = totalPageViews;
	}

	public String getTotalVisits() {
		return totalVisits;
	}

	public void setTotalVisits(String totalVisits) {
		this.totalVisits = totalVisits;
	}

	public String getUniqueVisitors() {
		return uniqueVisitors;
	}

	public void setUniqueVisitors(String uniqueVisitors) {
		this.uniqueVisitors = uniqueVisitors;
	}

	public String getDailySubscription() {
		return dailySubscription;
	}

	public void setDailySubscription(String dailySubscription) {
		this.dailySubscription = dailySubscription;
	}

	public String getWeeklySubscription() {
		return weeklySubscription;
	}

	public void setWeeklySubscription(String weeklySubscription) {
		this.weeklySubscription = weeklySubscription;
	}

	public String getMonthlySubscription() {
		return monthlySubscription;
	}

	public void setMonthlySubscription(String monthlySubscription) {
		this.monthlySubscription = monthlySubscription;
	}

	public String getDailyReSubscription() {
		return dailyReSubscription;
	}

	public void setDailyReSubscription(String dailyReSubscription) {
		this.dailyReSubscription = dailyReSubscription;
	}

	public String getWeeklyReSubscription() {
		return weeklyReSubscription;
	}

	public void setWeeklyReSubscription(String weeklyReSubscription) {
		this.weeklyReSubscription = weeklyReSubscription;
	}

	public String getMonthlyReSubscription() {
		return monthlyReSubscription;
	}

	public void setMonthlyReSubscription(String monthlyReSubscription) {
		this.monthlyReSubscription = monthlyReSubscription;
	}

	public String getTotalCategoryWiseDownloadRequest() {
		return totalCategoryWiseDownloadRequest;
	}

	public void setTotalCategoryWiseDownloadRequest(String totalCategoryWiseDownloadRequest) {
		this.totalCategoryWiseDownloadRequest = totalCategoryWiseDownloadRequest;
	}

	public String getAndroidGamesRequest() {
		return androidGamesRequest;
	}

	public void setAndroidGamesRequest(String androidGamesRequest) {
		this.androidGamesRequest = androidGamesRequest;
	}

	public String getHTML5GamesRequest() {
		return HTML5GamesRequest;
	}

	public void setHTML5GamesRequest(String hTML5GamesRequest) {
		HTML5GamesRequest = hTML5GamesRequest;
	}

	public String getTotalCategoryWiseDownloadSuccess() {
		return totalCategoryWiseDownloadSuccess;
	}

	public void setTotalCategoryWiseDownloadSuccess(String totalCategoryWiseDownloadSuccess) {
		this.totalCategoryWiseDownloadSuccess = totalCategoryWiseDownloadSuccess;
	}

	public String getAndroidGamesSuccess() {
		return androidGamesSuccess;
	}

	public void setAndroidGamesSuccess(String androidGamesSuccess) {
		this.androidGamesSuccess = androidGamesSuccess;
	}

	public String getHtml5GamesSuccess() {
		return html5GamesSuccess;
	}

	public void setHtml5GamesSuccess(String html5GamesSuccess) {
		this.html5GamesSuccess = html5GamesSuccess;
	}
	
	

	public String getsDate() {
		return sDate;
	}

	public void setsDate(String sDate) {
		this.sDate = sDate;
	}

	@Override
	public String toString() {
		return "ZamtelGamesMISEntity [id=" + id + ", date=" + date + ", totalBase=" + totalBase + ", activeBase="
				+ activeBase + ", totalSubscriptionRequestsReceived=" + totalSubscriptionRequestsReceived
				+ ", totalSecondConsentReceived=" + totalSecondConsentReceived + ", totalRequestProcessWAP="
				+ totalRequestProcessWAP + ", totalRequestProcessSMS=" + totalRequestProcessSMS
				+ ", totalRequestProcessUSSD=" + totalRequestProcessUSSD + ", totalSubscriptionRequestsProcessed="
				+ totalSubscriptionRequestsProcessed + ", totalGrossAdds=" + totalGrossAdds + ", voluntaryChurn="
				+ voluntaryChurn + ", wapMode=" + wapMode + ", smsMode=" + smsMode + ", ussdMode=" + ussdMode
				+ ", involuntaryChurn=" + involuntaryChurn + ", totalChurn=" + totalChurn + ", netAdds=" + netAdds
				+ ", totalRevenueInZK=" + totalRevenueInZK + ", subscriptionRevenue=" + subscriptionRevenue
				+ ", resubscriptionRevenue=" + resubscriptionRevenue + ", subRevenueFromSMS=" + subRevenueFromSMS
				+ ", subRevenueFromWAP=" + subRevenueFromWAP + ", subRevenueFromUSSD=" + subRevenueFromUSSD
				+ ", totalPageViews=" + totalPageViews + ", totalVisits=" + totalVisits + ", uniqueVisitors="
				+ uniqueVisitors + ", dailySubscription=" + dailySubscription + ", weeklySubscription="
				+ weeklySubscription + ", monthlySubscription=" + monthlySubscription + ", dailyReSubscription="
				+ dailyReSubscription + ", weeklyReSubscription=" + weeklyReSubscription + ", monthlyReSubscription="
				+ monthlyReSubscription + ", totalCategoryWiseDownloadRequest=" + totalCategoryWiseDownloadRequest
				+ ", androidGamesRequest=" + androidGamesRequest + ", HTML5GamesRequest=" + HTML5GamesRequest
				+ ", totalCategoryWiseDownloadSuccess=" + totalCategoryWiseDownloadSuccess + ", androidGamesSuccess="
				+ androidGamesSuccess + ", html5GamesSuccess=" + html5GamesSuccess + "]";
	}
	

	
	

}
